import './index.scss';

const logo = document.querySelector('.logo');
const cloud = document.querySelector('.cloud');
const cloudDescription = document.querySelector('.cloud_description');
const btnGroup = document.querySelector('.btn-group');
const btnTitle = document.querySelectorAll('.title');

let number = 50;

const scrollingEvent = (e) =>  {

  let scrollingPosition = e.deltaY || e.detail || e.wheelDelta;
  let delPercent = (scrollingPosition / 100) * 10;

  let result = number -= delPercent;

  if(result < 10) {
    number = 10;
    return;
  }
  else if(result > 50) {
    number = 50;
    return;
  }

  mobileLogoPosition(result);
  movingCloud(result);
}

const mobileLogoPosition = (result) => {
  let screenWidth = document.documentElement.clientWidth;

  if (screenWidth < 400) {
    movingLogo(result, 25);
  }

  else {
    movingLogo(result, 10);
  }
}

const movingLogo = (position, topHeightLogo) => {
  let result = position;

  if (position < topHeightLogo) {
    result = topHeightLogo;
  }

  else if ( position > 50 ) {
    result = 50
  }

  logo.style.top = result + '%';
}

const movingCloud = (position) => {

  let result = position;

  if(position < 10) {
    result = 10;
  }
  else if ( position > 40 ) {
    result = 40;
  }

  if (result === 10) {
    cloud.classList.add('add')
    movingCloudDescription(true);
  }
  else {
    cloud.classList.remove('add');
    movingCloudDescription(false);
  }
}

const movingCloudDescription = (position) => {
  addClass(position, cloudDescription)
  movingBtn(position);
}

const movingBtn = (position) => {
  addClass(position, btnGroup);
  movingBtnText(position);
}

const movingBtnText = (position) => {
  btnTitle.forEach( (item) => {
    addClass(position, item );
  });
}

const addClass = (position, element) => {
  if ( element ) {
    position ?
      element.classList.add('add') :
      element.classList.remove('add');
  }
}

const initialPositionLogo = () => {
  logo.style.top = '50%';
}

initialPositionLogo();


window.addEventListener('wheel', scrollingEvent);
window.addEventListener('resize', scrollingEvent);